﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Map : MonoBehaviour {


	public GameObject gameboard;

	/*

	private readonly Color NULL_COLOR = new Color(-1, -1, -1);*/

	private static Map _instance;

	// Use this for initialization
	void Awake() {
		_instance = this;
	}



	public static Square[,] init() {
		return _instance.drawMap();
	}

	private const int SQUARE_SIZE = 32;

	/* GRID[Y,X] */
	private readonly int[,] GRID_LAYOUT = new int[,] {
		{ 3, 0, 0, 0, 0, 0, 0, 0, 0, 3 },

		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },

		{ 2, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
		{ 2, 1, 1, 1, 1, 1, 1, 1, 1, 2 },

		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 },

		{ 3, 0, 0, 0, 0, 0, 0, 0, 0, 3 }
	};



	public static MeshRenderer render {
		get { return _instance.gameboard.GetComponent<MeshRenderer>(); }
	}


	/* */
	private void draw(ref Texture2D p_main, int p_x, int p_y, Color p_color) {
		Vec2 point = new Vec2(p_x * SQUARE_SIZE, p_y * SQUARE_SIZE);

		for(int x = 0; x < SQUARE_SIZE; x++) {
			for(int y = 0; y < SQUARE_SIZE; y++) {


				p_main.SetPixel(point.x + x, point.y + y, p_color);
			}
		}
	}


	private Square[,] drawMap() {
		if(GRID_LAYOUT == null)
			return null;

		/* Caching the width and height. */
		int width = GRID_LAYOUT.GetLength(0), height = GRID_LAYOUT.GetLength(1);

		/* Initializing the grid. */
		Square[,] grid = new Square[width, height];		

		/* Creating the grid texture. */
		Texture2D gridtex = new Texture2D(width * SQUARE_SIZE, height * SQUARE_SIZE);
		bool dark = false;
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				Color color = GRID_LAYOUT[x, y] == 0 ? new Color(0, 0, 0) : GRID_LAYOUT[x, y] == 2 ? new Color(.5f, .5f, .5f) : dark ? new Color(.25f, .25f, .25f) : new Color(.75f, .75f, .75f);

				grid[x, y] = new Square(new Vec3(x, y, SQUARE_SIZE), color);
				grid[x, y].draw(ref gridtex);
				grid[x, y].invalid = GRID_LAYOUT[x, y] == 0;
				grid[x, y].special = GRID_LAYOUT[x, y] == 2;
				grid[x, y].corner = GRID_LAYOUT[x, y] == 3;

				/* Assign team ownership to our special tiles. */
				if(grid[x, y].special || grid[x, y].corner)
					grid[x, y].team = y < 6 ? Team.white : Team.black;

				dark = !dark;
			}

			/* If the board is even, we have to make sure the squares are coloured diagonally. */
			if(height % 2 != 1)
				dark = !dark;
		}


		gridtex.filterMode = FilterMode.Point;
		gridtex.Apply();


		gameboard.GetComponent<MeshRenderer>().material.mainTexture = gridtex;
		gameboard.transform.localScale = new Vector3(width, height, 1);
		
		return grid;
	}

	private Color getTeamColor(Team p_team) {
		return p_team != Team.black ? new Color(.75f, .75f, .75f) : new Color(.25f, .25f, .25f);
		//return p_team == Team.black ? Color.black : new Color(.33f, .33f, .33f);
	}
}
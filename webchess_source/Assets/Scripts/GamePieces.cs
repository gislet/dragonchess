﻿using UnityEngine;
using System.Collections;

public class GamePieces : MonoBehaviour {

	public Texture2D team_background;
	public Color team_white, team_black;

	private static GamePieces _instance;

	public void Awake() {
		_instance = this;
	}


	public Wizard wizard;
	public Peasant peasant;
	public Knight knight;
	public King king;
	public Queen queen;
	public Dragon dragon;
	public Tower tower;
	public Cleric cleric;
	public PowerPeasant power_peasant;
	public Shield shield;


	/* Getters and Setters for duplicating the various gamepiece presets. */
	public static Peasant Peasant {
		get { return new Peasant(_instance.peasant); }
	}

	public static Knight Knight {
		get { return new Knight(_instance.knight); }
	}

	public static Queen Queen {
		get { return new Queen(_instance.queen); }
	}

	public static King King {
		get { return new King(_instance.king); }
	}

	public static Dragon Dragon {
		get { return new Dragon(_instance.dragon); }
	}

	public static Cleric Cleric {
		get { return new Cleric(_instance.cleric); }
	}

	public static Tower Tower {
		get { return new Tower(_instance.tower); }
	}

	public static PowerPeasant PowerPeasant {
		get { return new PowerPeasant(_instance.power_peasant); }
	}

	public static Shield Shield {
		get { return new Shield(_instance.shield); }
	}

	public static Wizard Wizard {
		get { return new Wizard(_instance.wizard); }
	}


	/* */
	public static Texture2D team {
		get { return _instance.team_background; }
	}


	/* */
	public static Color teamColor(Team p_team) {
		return p_team == Team.white ? _instance.team_white : _instance.team_black;
	}
}


public enum PawnType {
	Invalid,
	Peasant, Knight, Cleric,
	Tower, Dragon, Queen, King,
	Shield, Wizard, PowerPeasant
}

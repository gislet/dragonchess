﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPS_Meter : MonoBehaviour {

	public Text fps;

	private long _frames;
	private float _update;

	private const float UPDATE_FREQUENCY = 0.5f;

	// Use this for initialization
	void Start () {
		_update = UPDATE_FREQUENCY;
    }
	
	// Update is called once per frame
	void Update () {
		if(fps == null)
			return;

		_frames++;
		_update -= Time.deltaTime;

		if(_update <= 0) {
			_update = UPDATE_FREQUENCY;
			fps.text = "FPS: " + (_frames / UPDATE_FREQUENCY).ToString("f1");
			_frames = 0;
		}
    }
}

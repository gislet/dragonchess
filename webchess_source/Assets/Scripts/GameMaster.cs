﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


/* */
public class GameMaster : MonoBehaviour {


	public Texture2D accessible, squad_accessible;

	public Text cur_player_ui, pawn_placement_white, pawn_placement_black;

	public bool quick_start = false, simultaneous_move = true;

	private Team _cur_player;
	private Square[,] _grid;
	private Piece _piece;
	private State _game_state;
	private long _current_turn = 0;


	private static GameMaster _instance;



	private const int BLACK_PEASANT_UPGRADE_MARK = 4, WHITE_PEASANT_UPGRADE_MARK = 5;


	private PawnType[] _place_order = { PawnType.King, PawnType.Dragon, PawnType.Queen, PawnType.Shield, PawnType.Wizard, PawnType.Tower, PawnType.Cleric, PawnType.Knight };


	private readonly int[,] GRID_LINEUP = new int[,] {
		{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 6 },

		{ 0, 4, 1, 0, 0, 0, 0, 1, 4, 0 },
		{ 0, 3, 1, 0, 0, 0, 0, 1, 3, 0 },
		{ 0, 2, 1, 0, 0, 0, 0, 1, 2, 0 },
		{ 0, 8, 1, 0, 0, 0, 0, 1, 8, 0 },

		{ 5, 9, 1, 0, 0, 0, 0, 1, 9, 5 },
		{ 5, 9, 1, 0, 0, 0, 0, 1, 9, 5 },

		{ 0, 8, 1, 0, 0, 0, 0, 1, 8, 0 },
		{ 0, 2, 1, 0, 0, 0, 0, 1, 2, 0 },
		{ 0, 3, 1, 0, 0, 0, 0, 1, 3, 0 },
		{ 0, 4, 1, 0, 0, 0, 0, 1, 4, 0 },

		{ 7, 0, 0, 0, 0, 0, 0, 0, 0, 7 }
	};

	/*private readonly int[,] GRID_LINEUP = new int[,] {
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },

		{ 0, 4, 1, 0, 0, 0, 0, 1, 4, 0 },
		{ 0, 3, 1, 0, 0, 0, 0, 1, 3, 0 },
		{ 0, 2, 1, 0, 0, 0, 0, 1, 2, 0 },
		{ 0, 9, 1, 0, 0, 0, 0, 1, 9, 0 },

		{ 0, 8, 6, 0, 8, 0, 0, 1, 8, 0 },
		{ 0, 8, 1, 0, 0, 0, 0, 1, 8, 0 },

		{ 0, 9, 1, 0, 0, 0, 0, 1, 9, 0 },
		{ 0, 2, 1, 0, 0, 0, 0, 1, 2, 0 },
		{ 0, 3, 1, 0, 0, 0, 0, 1, 3, 0 },
		{ 0, 4, 1, 0, 0, 0, 0, 1, 4, 0 },

		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	};*/



	/* A late init that begins all the initialisations. */
	void Start () {
		_instance = this;

		game_state = State.init;
		init();
	}


	/* Performs all essential early initialisations. */
	private void init() {
		/* Sets the first player to be white. */
		_cur_player = Team.white;

		/* Creates the map. */
		_grid = Map.init();

		/* Looks for all special squares on the map, and make sure that they have their buddies found. */
		foreach(Square square in _grid) {
			if(square.special)
				square.findBuddies(_grid);
		}

		/* Finished initialising, proceeding to pawn placement. */
		game_state = State.pawn_placement;
		StartCoroutine(populate());
	}


	/* Called every frame. */
	void Update() {
		/* Telling the users what they're waiting on. */
		if(game_state != State.playing) {
			cur_player_ui.text = string.Format("{0}", game_state == State.pawn_placement ? "Waiting for players to place their pawns." : "Initializing...");
			return;
		}

		/* Calling all movement and gesture commands. */
		movement();

		/* Telling the users who's turn it currently is. */
		cur_player_ui.text = string.Format("{0}'s Turn", _cur_player == Team.black ? "Black" : "White");
	}


	/* Coroutine that handles the placement of the pawns, calling the necessary functions for our given game-mode. */
	private IEnumerator populate() {

		/* Places all pawns with predefined positions. */
		for(int x = 0; x < _grid.GetLength(0); x++) {
			for(int y = 0; y < _grid.GetLength(1); y++) {
				Team team = y > 5 ? Team.black : Team.white;

				if(!_grid[x, y].invalid) {
					PawnType pawn_type = (PawnType)GRID_LINEUP[x, y];

					/* Depending on game-mode, it places differently. */
					if(!quick_start ? pawn_type == PawnType.Peasant : pawn_type != PawnType.Invalid) {
						assignPawn(pawn_type, team, _grid[x, y]);
						_grid[x, y].draw(Map.render);
					}
				}
			}
		}

	
		if(!quick_start) {

			/* */
			int index_white = _place_order.Length - 1, index_black = _place_order.Length - 1;
			int white_count = 1, black_count = 1;
			PawnType next_white = _place_order[index_white];
			PawnType next_black = _place_order[index_black];

			/* */
			while((next_white != PawnType.Invalid || index_white >= 0) || (next_black != PawnType.Invalid || index_black >= 0)) {
				int width = _grid.GetLength(0), height = _grid.GetLength(1);
				Vec2 gpoint = getPoint();

				/* */
				if(next_black == PawnType.Invalid && index_black >= 0)
					next_black = _place_order[index_black];
				if(next_white == PawnType.Invalid && index_white >= 0)
					next_white = _place_order[index_white];

				pawn_placement_white.text = string.Format(next_white != PawnType.Invalid ? "{0}" : "{1}", "Next White: " + next_white, "White is Ready!");
				pawn_placement_black.text = string.Format(next_black != PawnType.Invalid ? "{0}" : "{1}", "Next Black: " + next_black, "Black is Ready!");

				/* */
				if(gpoint.x >= 0 && gpoint.y >= 0 && gpoint.x < width && gpoint.y < height) {
					Square square = _grid[gpoint.x, gpoint.y];

					if(square.occupant == null && !square.invalid) {
						Team team = gpoint.y < WHITE_PEASANT_UPGRADE_MARK ? Team.white : gpoint.y > BLACK_PEASANT_UPGRADE_MARK ? Team.black : Team.none;

						PawnType pawn_type = team == Team.white ? next_white : next_black;

						/* */
						if(Input.GetMouseButtonDown(0) && ((team == Team.black && ((gpoint.y == (height - 1) && index_black < 3) || (gpoint.y != (height - 1) && index_black >= 3))) ||
															(team == Team.white && ((gpoint.y == 0 && index_white < 3) || (gpoint.y != 0 && index_white >= 3))))) {
							Pawn pawn = assignPawn(pawn_type, team, square);
							square.draw(Map.render);

							/* */
							if(team == Team.black) {
								next_black = PawnType.Invalid;
								index_black--;
								if(index_black < 3 && black_count == 1) {
									index_black = _place_order.Length - 1;
									black_count++;
								}
							} else {
								next_white = PawnType.Invalid;
								index_white--;
								if(index_white < 3 && white_count == 1) {
									index_white = _place_order.Length - 1;
									white_count++;
								}
							}
						}
					}
				}

				yield return null;
			}
		}


		pawn_placement_white.text = "";
		pawn_placement_black.text = "";

		/* Pawn placement is complete. The users can now play the game! */
		game_state = State.playing;
	}


	/* Finding which pawn to place on our square, and setting it up. */
	private Pawn assignPawn(PawnType p_type, Team p_team, Square p_square) {
		Pawn pawn;

		switch(p_type) {
		case PawnType.Peasant:
			pawn = GamePieces.Peasant;
			break;
		case PawnType.Knight:
			pawn = GamePieces.Knight;
			break;
		case PawnType.King:
			pawn = GamePieces.King;
			break;
		case PawnType.Queen:
			pawn = GamePieces.Queen;
			break;
		case PawnType.Wizard:
			pawn = GamePieces.Wizard;
			break;
		case PawnType.Tower:
			pawn = GamePieces.Tower;
			break;
		case PawnType.PowerPeasant:
			pawn = GamePieces.PowerPeasant;
			break;
		case PawnType.Shield:
			pawn = GamePieces.Shield;
			break;
		case PawnType.Dragon:
			pawn = GamePieces.Dragon;
			break;
		case PawnType.Cleric:
			pawn = GamePieces.Cleric;
            break;
		default:
			Debug.LogError(string.Format("There is no assigned protocol for spawning pawn of type {0}.", p_type.ToString()));
			return null;
		}

		/* Making sure our pawn clone is introduced correctly, by giving it a team and a square to stand on. */
		pawn.team = p_team;
		p_square.occupant = pawn;

		return pawn;
	}


	/* Aquiring the point on the gameboard the cursor is. */
	private Vec2 getPoint() {
		int width = _grid.GetLength(0), height = _grid.GetLength(1);
		Vector3 worldpoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);		
		Vec2 gpoint = new Vec2(Mathf.FloorToInt(worldpoint.x + (width / 2)),
										Mathf.FloorToInt(worldpoint.y + (height / 2)));

		return gpoint;
	}


	/* */
	private void pickUpPawn(Vec2 p_bounds, Vec2 p_point) {
		if(Globals.checkBounds(p_bounds, p_point)) {
			Square square = _grid[p_point.x, p_point.y];

			if(square.occupant != null) {
				if(square.occupant.team == _cur_player || simultaneous_move) {
					_piece = new Piece();

					_piece.pawn = square.occupant;
					_piece.origin = p_point;
					_piece.moves = square.occupant.movement(_grid, p_point);

					foreach(Vec2 v in _piece.moves) {
						_grid[v.x, v.y].overlap(Map.render, accessible);
					}
				}
			}
		}
	}


	/* */
	private void pickUpSquadPawn(Vec2 p_bounds, Vec2 p_point) {
		if(Globals.checkBounds(p_bounds, p_point)) {
			Square square = _grid[p_point.x, p_point.y];

			if(square.occupant != null) {
				if(_piece != null) {
					if((_piece.origin.y == p_point.y) && (_piece.origin.x + 1 == p_point.x || _piece.origin.x - 1 == p_point.x)) {
						if(_piece.pawn.GetType() == typeof(Peasant)) {
							if(square.occupant.GetType() == typeof(Peasant)) {
								if(_piece.pawn != square.occupant && _piece.pawn.team == square.occupant.team) {
									Piece squad_member = ((Peasant)_piece.pawn).squad_member;

									if(squad_member != null) {
										if(squad_member.pawn != square.occupant) {
											foreach(Vec2 v in squad_member.moves) {
												_grid[v.x, v.y].draw(Map.render);
											}

											((Peasant)_piece.pawn).squad_member.pawn = square.occupant;
											((Peasant)_piece.pawn).squad_member.origin = p_point;
											((Peasant)_piece.pawn).squad_member.moves = square.occupant.movement(_grid, p_point);

											foreach(Vec2 v in ((Peasant)_piece.pawn).squad_member.moves) {
												_grid[v.x, v.y].overlap(Map.render, squad_accessible);
											}
										}
									} else {
										((Peasant)_piece.pawn).squad_member = new Piece();

										((Peasant)_piece.pawn).squad_member.pawn = square.occupant;
										((Peasant)_piece.pawn).squad_member.origin = p_point;
										((Peasant)_piece.pawn).squad_member.moves = square.occupant.movement(_grid, p_point);

										foreach(Vec2 v in ((Peasant)_piece.pawn).squad_member.moves) {
											_grid[v.x, v.y].overlap(Map.render, squad_accessible);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}


	/* */
	private void dropPawn(Vec2 p_bounds, Vec2 p_point) {
		if(_piece != null) {
			foreach(Vec2 v in _piece.moves) {
				if(v.x == p_point.x && v.y == p_point.y) {
					/* If we are moving onto a square with an occupant from our team, 
						* we're most likely supposed to switch positions.*/
					if(_grid[v.x, v.y].occupant == null ? false : _grid[v.x, v.y].occupant.team == _piece.pawn.team ? true : false) {
						_grid[_piece.origin.x, _piece.origin.y].occupant = _grid[v.x, v.y].occupant;
					} else {
						_grid[_piece.origin.x, _piece.origin.y].occupant = null;
					}

					/* The peasant is a special pawn that can move another peasant pawn that is next to it horizontally. */
					if(_piece.pawn.GetType() == typeof(Peasant)) {
						handleSquads(p_bounds, p_point, v);
					}

					/* We then got to move our pawn to the new position and update both the original
						* and new square. */
					_grid[v.x, v.y].occupant = _piece.pawn;
					_grid[v.x, v.y].draw(Map.render);
					_grid[_piece.origin.x, _piece.origin.y].draw(Map.render);
					_cur_player = _cur_player == Team.black ? Team.white : Team.black;
					_current_turn++;
				} else {

					/* Making sure we remove any clutter from each square that is a legal move. */
					_grid[v.x, v.y].draw(Map.render);
				}
			}

			
			clearSquads();
			_piece = null;

		}
	}


	/* Making sure that the squad variable is empty when it's supposed to. */
	private void clearSquads() {
		if(_piece.pawn.GetType() == typeof(Peasant)) {
			if(((Peasant)_piece.pawn).squad_member != null) {
				Piece squad_member = ((Peasant)_piece.pawn).squad_member;

				/* Updating the map to remove possible movement indicators. */
				foreach(Vec2 sv in squad_member.moves) {
					_grid[sv.x, sv.y].draw(Map.render);
				}
			}

			((Peasant)_piece.pawn).squad_member = null;
		}
	}


	/* */
	private void handleSquads(Vec2 p_bounds, Vec2 p_point, Vec2 p_move) {
		/* Additionally, the peasant has a special case where it changes to a new type of pawn when going past
		 * the middle of the board and fully into enemy territory. */

		if((_piece.pawn.team == Team.black && p_move.y < BLACK_PEASANT_UPGRADE_MARK)
			|| (_piece.pawn.team == Team.white && p_move.y > WHITE_PEASANT_UPGRADE_MARK)) {
			Piece squadie = ((Peasant)_piece.pawn).squad_member;
			_piece.pawn = assignPawn(PawnType.PowerPeasant, _piece.pawn.team, _grid[_piece.origin.x, _piece.origin.y]);
			((Peasant)_piece.pawn).squad_member = squadie;
			_grid[_piece.origin.x, _piece.origin.y].occupant = null;

			if(((Peasant)_piece.pawn).squad_member != null) {
				((Peasant)_piece.pawn).squad_member.pawn = assignPawn(PawnType.PowerPeasant, _piece.pawn.team, _grid[((Peasant)_piece.pawn).squad_member.origin.x, ((Peasant)_piece.pawn).squad_member.origin.y]);
				_grid[((Peasant)_piece.pawn).squad_member.origin.x, ((Peasant)_piece.pawn).squad_member.origin.y].occupant = null;
			}
		}

		if(((Peasant)_piece.pawn).squad_member != null) {
			Piece squad_member = ((Peasant)_piece.pawn).squad_member;

			foreach(Vec2 sv in squad_member.moves) {
				_grid[sv.x, sv.y].draw(Map.render);
			}

			if(_piece.origin.x != p_point.x || _piece.origin.y != p_point.y) {
				int offset_x = (p_point.x - _piece.origin.x);
				int offset_y = (p_point.y - _piece.origin.y);

				_grid[squad_member.origin.x - offset_x, squad_member.origin.y + offset_y].occupant = squad_member.pawn;
				_grid[squad_member.origin.x - offset_x, squad_member.origin.y + offset_y].draw(Map.render);

				_grid[squad_member.origin.x, squad_member.origin.y].occupant = null;
				_grid[squad_member.origin.x, squad_member.origin.y].draw(Map.render);
			}
		}

		((Peasant)_piece.pawn).squad_member = null;
	}


	/* Calling all movement and gesture commands. */
	private void movement() {
		Vec2 bounds = new Vec2(_grid.GetLength(0), _grid.GetLength(1));

		/* Aquiring the point on the gameboard the cursor is. */
		Vec2 gpoint = getPoint();
		

		/* When the player holds the button, find all allowed moves for the gamepiece
		 * that occupies the square. */
		if(Input.GetMouseButtonDown(0)) {
			pickUpPawn(bounds, gpoint);
		}


		/* */
		if(Input.GetMouseButton(0) && _current_turn > 0) {
			pickUpSquadPawn(bounds, gpoint);
		}


		/* When the player releases the button, make sure the map is up to date. Also
		 * move the _piece to its new position if the position is valid. */
		if(Input.GetMouseButtonUp(0) /*||  touch */) {
			dropPawn(bounds, gpoint);
		}
	}


	/* Global access to the game's state. */
	public static State game_state {
		get { return _instance._game_state; }
		set { _instance._game_state = value; }
	}
}

public enum State {
	init, pawn_placement, playing
}

public class Piece {
	public Pawn pawn;
	public Vec2 origin;
	public Vec2[] moves;
}

public enum Team {
	none, white, black
}

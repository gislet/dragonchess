﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


/* */
public class Square {

	/* */
	private Pawn _occupant;
	public Vec3 point;
	public bool invalid;
	public bool special;
	public bool corner;

	public Team team;

	public Vec2 origin;
	private List<Square> _buddies;

	/* Visual */
	public Color[] texture;



	/* Square Constructor. Accepts the draw starting positions and a texture for us to cache . */
	public Square(Vec3 p_point, Texture2D p_tex) {
		_buddies = new List<Square>();
		point = p_point;

		/* We store our texture in the form of an array of Colours. */
		texture = p_tex.GetPixels();
	}


	/* Square Constructor. Accepts the draw starting positions and an array of Colours. */
	public Square(Vec3 p_point, Color p_color) {
		_buddies = new List<Square>();
		point = p_point;

		/* We allocate our Colour array and add in the received colours. */
		texture = new Color[point.z * point.z];
		for(int i = 0; i < texture.Length; i++)
			texture[i] = p_color;
	}


	/* Looking for squares in our immediate area that are also tagged as special. */
	public void findBuddies(Square[,] p_grid) {

		origin = new Vec2(point.x, point.y);

		for(int x = -1; x <= 1; x++) {
			for(int y = -1; y <= 1; y++) {
				if(point.x + x >= 0 && point.y + y >= 0 && point.x + x < p_grid.GetLength(0) && point.y + y < p_grid.GetLength(1)) {
					if(x != 0 || y != 0) {
						Square square = p_grid[point.x + x, point.y + y];

						/* As long as the square we found isn't us, and it is indeed special, we add it to our list. */
						if(square.special) {
							_buddies.Add(square);
						}
					}
				}
			}
		}
		
		/* Finally we change the draw points our each buddy square to be as close to the middle as possible. */
		foreach(Square buddy in _buddies) {
			buddy.draw(Map.render, true);

			if(buddy.point.x < point.x)
				point.x = buddy.point.x;

			if(buddy.point.y < point.y)
				point.y = buddy.point.y;
		}
	}
	

	/* Adds a texture to the already existing texture on the received renderer. */
	public void overlap(MeshRenderer p_renderer, Texture2D p_overlap, bool p_team_shade = false) {
		Texture2D main = p_renderer.material.mainTexture as Texture2D;
		Color[] tex = p_overlap.GetPixels();
		Color[] curtex = main.GetPixels(Mathf.RoundToInt((point.x + (_buddies.Count / 2f)) * point.z), point.y * point.z, point.z, point.z);

		/* Performing a colour lerp to ensure that transparencies are kept. */
		for(int i = 0; i < tex.Length; i++) {
			if(tex[i].a > 0 && p_team_shade) {
				tex[i] = Color.Lerp(tex[i], GamePieces.teamColor(occupant.team), .5f);
			}
			tex[i] = Color.Lerp(curtex[i], tex[i], tex[i].a);
		}
		
		/* Applies the finished texture. */
		main.SetPixels(Mathf.RoundToInt((point.x + (_buddies.Count / 2f)) * point.z), point.y * point.z, point.z, point.z, tex);
		applyTexture(p_renderer, main);
	}


	/* Draws our cached texture and applies it to the received renderer, additionally draws any pawn that might be
	 * occupying our square unless otherwise stated. */
	public void draw(MeshRenderer p_renderer, bool p_clean = false) {
		Texture2D tex = p_renderer.material.mainTexture as Texture2D;
		tex.SetPixels(Mathf.RoundToInt((point.x + (_buddies.Count / 2f)) * point.z), point.y * point.z, point.z, point.z, texture);
		applyTexture(p_renderer, tex);

		if(occupant != null && !p_clean) {
			overlap(p_renderer, GamePieces.team, true);
			overlap(p_renderer, occupant.icon);
		}		
	}


	/* Draws our cached texture on the received texture. */
	public void draw(ref Texture2D p_main) {
		p_main.SetPixels(Mathf.RoundToInt((point.x + (_buddies.Count / 2f)) * point.z), point.y * point.z, point.z, point.z, texture);
	}


	/* Applies the received texture and adds it to the received renderer. */
	private void applyTexture(MeshRenderer p_renderer, Texture2D p_tex) {
		p_tex.Apply();
		p_renderer.material.mainTexture = p_tex;
	}


	/* Converts the buddy list of squares, into an array of Vec2s. */
	public Vec2[] buddies {
		get {
			List<Vec2> buds = new List<Vec2>();

			foreach(Square buddy in _buddies) {
				buds.Add(buddy.origin);
			}

			return buds.ToArray();
		}
	}


	/* Getting and setting the occupant. */
	public Pawn occupant {
		get { return _occupant; }
		set {
			/* In the case that square is a special square, we make sure to set the 
			 * value to be the same across the all. */
			if(special) {
				_occupant = value;

				/* We want to make sure that only the focused square
				 * those the updating. */
				foreach(Square buddy in _buddies) {
					if(buddy.occupant == value)
						return;
				}

				foreach(Square buddy in _buddies) {
					buddy.occupant = value;
                }
			} else {
				_occupant = value;
			}
		}
	}
}
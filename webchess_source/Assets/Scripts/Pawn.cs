﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/* Base Pawn class, has to be inherited for the movement function to work. Class
 * consists of many functions that perform menial tasks. */
[System.Serializable]
public class Pawn {


	public int move_limit;
	public Texture2D icon;

	protected bool _searching = false;
	private Team _team;


	/* Standard empty constructor. */
	public Pawn() { }

	/* Gets called when duplicating a Pawn preset. */
	public Pawn(Pawn p_piece) : base() {
		move_limit = p_piece.move_limit;
		icon = p_piece.icon;
	}


	/* Method that is unique from class to class, where its primary function is to
	 * find all legal moves from the given position on the grid. */
	public virtual Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		return null;
	}
	

	/* Aquiring squares that are legal move spaces. */
	protected Vec2[] addSquare(Square[,] p_grid, Vec2 p_origin, Vec2 p_square) {
		List<Vec2> points = new List<Vec2>();

		/* Making sure we're not looking at the square we're standing on. */
		if(p_square.x != p_origin.x || p_square.y != p_origin.y) {

			/* Making sure we're within the grid bounds. */
			if(withinBoundaries(new Vec2(p_grid.GetLength(0), p_grid.GetLength(1)), p_square)) { 
			//if(p_square.x >= 0 && p_square.y >= 0 && p_square.x < p_grid.GetLength(0) && p_square.y < p_grid.GetLength(1)) {
				Square square = p_grid[p_square.x, p_square.y];

				/* Checking to see that the square can be used. */
				if(!square.invalid) {

					/* If the square is occupied, we want to make sure it's not a teammate who's occupying it. */
					if(square.occupant == null ? true : square.occupant.team == team ? false : true) {

						/* If we're standing on a square that is considered special, we cannot move to
						 * a square that is also considered special. */
						if(!square.special || !p_grid[p_origin.x, p_origin.y].special) {

							/* All is good, we add the point as a valid move. */
							points.Add(new Vec2(p_square.x, p_square.y));

							/* Additionally if we're standing on a special square, we add in
							 * all of the squares associated with it. */
							if(square.special) {
								foreach(Vec2 buddy in square.buddies) {
									points.Add(buddy);
								}
							}
						}
					}
				}
			}
		}

		/* Returns all new legal moves found. */
		return points.ToArray();
	}


	/* Making sure there are no additional entires of the same Vector. */
	protected Vec2[] removeOverlap(Vec2[] p_old, Vec2[] p_new) {
		List<Vec2> points = new List<Vec2>();

		/* We go through each Vector in the new list which should contain
		 * all entires in the old, and possible duplicates. */
		foreach(Vec2 m in p_new) {
			bool exists = false;

			foreach(Vec2 p in p_old) {
				if(m.x == p.x && m.y == p.y) {
					exists = true;
					break;
				}
			}

			/* If we didn't find one that exists, add that to our new list. */
			if(!exists)
				points.Add(m);
		}

		/* Return the list of all actual new moves that have been found. */
		return points.ToArray();
	}


	/* Checking the focused Vector for any possible moves, cleaning up possible overlaps, and then returning
	 * the new and updated list. */
	protected List<Vec2> findMoves(Square[,] p_grid, List<Vec2> p_cur_moves, Vec2 p_origin, Vec2 p_focus) {
		List<Vec2> new_moves = new List<Vec2>();

		new_moves.AddRange(removeOverlap(p_cur_moves.ToArray(), addSquare(p_grid, p_origin, p_focus)));
		new_moves.AddRange(p_cur_moves);

		return new_moves;
	}


	/* This method gets called when the origin square is a special square. It goes through all the squares tied to it,
	 * or the "buddy" squares, and does a movement check on them as well to aquire all legal moves. */
	protected List<Vec2> inspectBuddies(Square[,] p_grid, List<Vec2> p_cur_moves, Vec2 p_origin) {
		/* In case the method got called by accident, return. */
		if(!p_grid[p_origin.x, p_origin.y].special)
			return p_cur_moves;

		/* Enabling the search bool so that we don't end up with an infinite loop by calling the movement funciton. */
		_searching = true;

		/* We look up each buddy and call the movement function. */
		foreach(Vec2 b in p_grid[p_origin.x, p_origin.y].buddies) {
			List<Vec2> more = new List<Vec2>();
			more.AddRange(movement(p_grid, b));

			/* We go through all of our new Vectors to see if there is any overlapping with our current
			 * list of Vectors. */
			foreach(Vec2 m in more) {
				bool exists = false;

				foreach(Vec2 p in p_cur_moves) {
					if(m.x == p.x && m.y == p.y) {
						exists = true;
						break;
					}
				}

				/* If our new Vector doesn't exist in our current list of moves, we add it. */
				if(!exists)
					p_cur_moves.Add(m);
			}
		}

		/* Disabling the search lock and returning the final list of all legal moves. */
		_searching = false;
		return p_cur_moves;
	}


	/* */
	protected List<Vec2> findDirectionalMoves(Square[,] p_grid, Vec2 p_origin, List<Vec2> p_cur_moves, int p_move_length, bool p_horizontal, bool p_vertical, bool p_diagonal) {
		List<Vec2> points = new List<Vec2>();

		bool[] b = { true };
		Vec2[] v = { new Vec2() };


		/* Getting the correct length on the bool array. */
		if(p_horizontal && p_vertical && p_diagonal)
			b = new bool[] { true, true, true, true, true, true, true, true };
		else if((p_horizontal && p_diagonal) || (p_vertical && p_diagonal))
			b = new bool[] { true, true, true, true, true, true };
		else if((p_horizontal && p_vertical) || p_diagonal)
			b = new bool[] { true, true, true, true };
		else if(p_horizontal || p_vertical)
			b = new bool[] { true, true };


		/* Horizontal Moves. */
		for(int i = 1; i <= p_move_length; i++) {

			/* Making sure we're about to use the correct vector functions. */
			if(p_horizontal && p_vertical && p_diagonal) {
				v = new Vec2[] {
					new Vec2(p_origin.x + i, p_origin.y),
					new Vec2(p_origin.x - i, p_origin.y),
					new Vec2(p_origin.x, p_origin.y + i),
					new Vec2(p_origin.x, p_origin.y - i),

					new Vec2(p_origin.x + i, p_origin.y + i),
					new Vec2(p_origin.x - i, p_origin.y - i),
					new Vec2(p_origin.x + i, p_origin.y - i),
					new Vec2(p_origin.x - i, p_origin.y + i)
				};
			} else if((p_horizontal && p_diagonal) || (p_vertical && p_diagonal)) {
				if(p_horizontal) {
					v = new Vec2[] {
						new Vec2(p_origin.x + i, p_origin.y),
						new Vec2(p_origin.x - i, p_origin.y),

						new Vec2(p_origin.x + i, p_origin.y + i),
						new Vec2(p_origin.x - i, p_origin.y - i),
						new Vec2(p_origin.x + i, p_origin.y - i),
						new Vec2(p_origin.x - i, p_origin.y + i)
					};
				} else {
					v = new Vec2[] {
						new Vec2(p_origin.x, p_origin.y + i),
						new Vec2(p_origin.x, p_origin.y - i),

						new Vec2(p_origin.x + i, p_origin.y + i),
						new Vec2(p_origin.x - i, p_origin.y - i),
						new Vec2(p_origin.x + i, p_origin.y - i),
						new Vec2(p_origin.x - i, p_origin.y + i)
					};
				}
			} else if((p_horizontal && p_vertical) || p_diagonal) {
				if(p_diagonal) {
					v = new Vec2[] {
						new Vec2(p_origin.x + i, p_origin.y + i),
						new Vec2(p_origin.x - i, p_origin.y - i),
						new Vec2(p_origin.x + i, p_origin.y - i),
						new Vec2(p_origin.x - i, p_origin.y + i)
					};
				} else {
					v = new Vec2[] {
						new Vec2(p_origin.x + i, p_origin.y),
						new Vec2(p_origin.x - i, p_origin.y),
						new Vec2(p_origin.x, p_origin.y + i),
						new Vec2(p_origin.x, p_origin.y - i)
					};
				}
			} else if(p_horizontal || p_vertical) {
				if(p_horizontal) {
					v = new Vec2[] {
						new Vec2(p_origin.x + i, p_origin.y),
						new Vec2(p_origin.x - i, p_origin.y)
					};
				} else {
					v = new Vec2[] {
						new Vec2(p_origin.x, p_origin.y + i),
						new Vec2(p_origin.x, p_origin.y - i)
					};
				}
			}

			/* Going through the selections and finding our legal moves. */
			for(int g = 0; g < v.Length; g++) {
				if(withinBoundaries(new Vec2(p_grid.GetLength(0), p_grid.GetLength(1)), v[g]) && b[g]) {
					if(p_grid[v[g].x, v[g].y].occupant != null || p_grid[v[g].x, v[g].y].invalid) {
						b[g] = false;
					}
					points = findMoves(p_grid, points, p_origin, v[g]);
				}
			}
		}

		p_cur_moves.AddRange(removeOverlap(p_cur_moves.ToArray(), points.ToArray()));

		return p_cur_moves;
	}


	/* Checks to see if the square the pawn is standing on, has any special behaviour. */
	public bool isSpecial(Square p_square) {
		if(p_square.special || p_square.corner)
			return true;

		return false;
	}


	/* Retrieves all special move behaviour. */
	public List<Vec2> findSpecialMoves(Square[,] p_grid, List<Vec2> p_cur_moves, Vec2 p_origin) {
		Square square = p_grid[p_origin.x, p_origin.y];
		List<Vec2> moves = new List<Vec2>();

		/* Regardless of team ownership, the special tiles can be occupied by either. */
		if(square.special) {
			p_cur_moves = inspectBuddies(p_grid, p_cur_moves, p_origin);
		}

		/* If the square is owned by this pawn, it shall also be able to move from the corners
		 * to its special tile, and back. */
		if(square.team == team) {
			if(square.special) {
				Vec2[] white_corners = {
					new Vec2(),
					new Vec2(p_grid.GetLength(0) - 1, 0)
				};

				Vec2[] black_corners = {
					new Vec2(0, p_grid.GetLength(1) - 1),
					new Vec2(p_grid.GetLength(0) - 1, p_grid.GetLength(1) - 1)
				};

				moves.AddRange(team == Team.white ? white_corners : black_corners);
			} else {
				Vec2[] white_special = {
					new Vec2(p_grid.GetLength(0) / 2, 0),
					new Vec2((p_grid.GetLength(0) / 2) - 1, 0)
				};
				Vec2[] black_special = {
					new Vec2(p_grid.GetLength(0) / 2, p_grid.GetLength(1) - 1),
					new Vec2((p_grid.GetLength(0) / 2) - 1, p_grid.GetLength(1) - 1)
				};

				moves.AddRange(team == Team.white ? white_special : black_special);
			}
		}

		List<Vec2> correction = new List<Vec2>();
		foreach(Vec2 v in moves) {
			if(p_grid[v.x, v.y].occupant == null) {
				correction.Add(v);
			}
		}
		moves = correction;

		p_cur_moves.AddRange(removeOverlap(p_cur_moves.ToArray(), moves.ToArray()));

		return p_cur_moves;
	}


	/* Makes sure that the second point is within the first point given. */
	public bool withinBoundaries(Vec2 p_boundary, Vec2 p_point) {
		if(p_point.x >= 0 && p_point.y >= 0 && p_point.x < p_boundary.x && p_point.y < p_boundary.y)
			return true;

		return false;
	}


	/* Getter and Setter for the team of this pawn. */
	public Team team {
		get { return _team; }
		set { _team = value; }
	}
}



/* The Peasant Class, inherits from the base Pawn Class. */
[System.Serializable]
public class Peasant : Pawn {

	private Piece _squad_member;

	private int _move_direction = 0;

	private Vec2[] _moves = {
		new Vec2(1, 1),
		new Vec2(-1, 1)
	};

	/* Peasant Constructor, calling the base constructor. */
	public Peasant(Pawn p_piece) : base(p_piece) {}


	/* Movement code specifically for the Peasant. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();

		/* Making sure the move direction is set. */
		if(_move_direction == 0)
			_move_direction = team == Team.white ? 1 : -1;

		Vec2 v = new Vec2(p_origin.x, p_origin.y + _move_direction);
		points = findMoves(p_grid, points, p_origin, v);

		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}


	/* */
	public Piece squad_member {
		get { return _squad_member; }
		set { _squad_member = value; }
	}
}



/* The Knight Class, inherits from the base Pawn Class. */
[System.Serializable]
public class Knight : Pawn {


	private const int LONG = 2;
	private const int SHORT = 1;


	/* All relative legal moves that the knight can perform. */
	private Vec2[] _moves = {
		/* Upwards */
		new Vec2(SHORT, LONG),
		new Vec2(-SHORT, LONG),
		new Vec2(LONG, SHORT),
		new Vec2(-LONG, SHORT),

		/* Downwards */
		new Vec2(SHORT, -LONG),
		new Vec2(-SHORT, -LONG),
		new Vec2(LONG, -SHORT),
		new Vec2(-LONG, -SHORT),
	};


	/* Knights Constructor, calling the base constructor. */
	public Knight(Pawn p_piece) : base(p_piece) {}


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();

		for(int i = 0; i < _moves.Length; i++) {
			Vec2 v = new Vec2(p_origin.x + _moves[i].x, p_origin.y + _moves[i].y);

			points = findMoves(p_grid, points, p_origin, v);
		}

		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}


/* The PowerPeasant Class, inherits from the base Pawn Class. */
[System.Serializable]
public class PowerPeasant : Peasant {

	/* PowerPeasant Constructor, calling the base constructor. */
	public PowerPeasant(Pawn p_piece) : base(p_piece) { }


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();


		points = findDirectionalMoves(p_grid, p_origin, points, move_limit, true, true, true);


		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}


/* The King Class, inherits from the base Pawn Class. */
[System.Serializable]
public class King : Pawn {

	/* King Constructor, calling the base constructor. */
	public King(Pawn p_piece) : base(p_piece) { }


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();

		points = findDirectionalMoves(p_grid, p_origin, points, move_limit, true, true, true);

		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}


/* The Queen Class, inherits from the base Pawn Class. */
[System.Serializable]
public class Queen : Pawn {

	/* Queen Constructor, calling the base constructor. */
	public Queen(Pawn p_piece) : base(p_piece) { }


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();

		points = findDirectionalMoves(p_grid, p_origin, points, move_limit, true, true, true);

		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}


/* The Tower Class, inherits from the base Pawn Class. */
[System.Serializable]
public class Tower : Pawn {

	/* Tower Constructor, calling the base constructor. */
	public Tower(Pawn p_piece) : base(p_piece) { }


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();
		
		points = findDirectionalMoves(p_grid, p_origin, points, move_limit, true, true, false);

		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}


/* The Cleric Class, inherits from the base Pawn Class. */
[System.Serializable]
public class Cleric : Pawn {

	/* Cleric Constructor, calling the base constructor. */
	public Cleric(Pawn p_piece) : base(p_piece) { }


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();


		points = findDirectionalMoves(p_grid, p_origin, points, move_limit, false, false, true);


		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}


/* The Dragon Class, inherits from the base Pawn Class. */
[System.Serializable]
public class Dragon : Pawn {


	private const int LONG = 2;
	private const int SHORT = 1;


	/* All relative legal moves that the dragon can perform. */
	private Vec2[] _moves = {
		/* Upwards */
		new Vec2(SHORT, LONG),
		new Vec2(-SHORT, LONG),
		new Vec2(LONG, SHORT),
		new Vec2(-LONG, SHORT),

		/* Downwards */
		new Vec2(SHORT, -LONG),
		new Vec2(-SHORT, -LONG),
		new Vec2(LONG, -SHORT),
		new Vec2(-LONG, -SHORT),
	};


	/* Dragon Constructor, calling the base constructor. */
	public Dragon(Pawn p_piece) : base(p_piece) { }


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();


		for(int i = 0; i < _moves.Length; i++) {
			Vec2 v = new Vec2(p_origin.x + _moves[i].x, p_origin.y + _moves[i].y);
			int cur_moves = points.Count;

			while(true) {
				if(!withinBoundaries(new Vec2(p_grid.GetLength(0), p_grid.GetLength(1)), v))
					break;
				if(p_grid[v.x, v.y].invalid)
					break;

				points = findMoves(p_grid, points, p_origin, v);

				if(p_grid[v.x, v.y].occupant != null)
					break;

				v = new Vec2(v.x + _moves[i].x, v.y + _moves[i].y);
			}
		}


		points = findDirectionalMoves(p_grid, p_origin, points, move_limit, true, true, true);


		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}


/* The Shield Class, inherits from the base Pawn Class. */
[System.Serializable]
public class Shield : Pawn {

	private const int LONG = 2;

	private Vec2[] _moves = {
		/* Upwards */
		new Vec2(LONG, 0),
		new Vec2(0, LONG),
		new Vec2(0, -LONG),
		new Vec2(-LONG, 0)
	};


	/* Shield Constructor, calling the base constructor. */
	public Shield(Pawn p_piece) : base(p_piece) { }


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();


		points = findDirectionalMoves(p_grid, p_origin, points, move_limit, true, true, true);

		for(int i = 0; i < _moves.Length; i++) {
			Vec2 v = new Vec2(p_origin.x + _moves[i].x, p_origin.y + _moves[i].y);
			points = findMoves(p_grid, points, p_origin, v);
		}


		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}


/* The Wizard Class, inherits from the base Pawn Class. */
[System.Serializable]
public class Wizard : Pawn {


	private const int LONG = 3;
	private const int MEDIUM = 2;
	private const int SHORT = 1;


	/* All relative legal moves that the wizard can perform. */
	private Vec2[] _moves = {
		/* Upwards */
		new Vec2(SHORT, LONG),
		new Vec2(-SHORT, LONG),
		new Vec2(MEDIUM, MEDIUM),
		new Vec2(-MEDIUM, MEDIUM),
		new Vec2(LONG, SHORT),
		new Vec2(-LONG, SHORT),

		/* Downwards */
		new Vec2(SHORT, -LONG),
		new Vec2(-SHORT, -LONG),
		new Vec2(MEDIUM, -MEDIUM),
		new Vec2(-MEDIUM, -MEDIUM),
		new Vec2(LONG, -SHORT),
		new Vec2(-LONG, -SHORT),
	};


	/* Wizard Constructor, calling the base constructor. */
	public Wizard(Pawn p_piece) : base(p_piece) { }


	/* Movement code specifically for the Knight. */
	public override Vec2[] movement(Square[,] p_grid, Vec2 p_origin) {
		List<Vec2> points = new List<Vec2>();


		for(int i = 0; i < _moves.Length; i++) {
			Vec2 v = new Vec2(p_origin.x + _moves[i].x, p_origin.y + _moves[i].y);
			points = findMoves(p_grid, points, p_origin, v);
		}


		/* Finding all legal moves if our pawn is on a special square. */
		if(isSpecial(p_grid[p_origin.x, p_origin.y]) && !_searching) {
			points = findSpecialMoves(p_grid, points, p_origin);
		}

		return points.ToArray();
	}
}
﻿using UnityEngine;
using System.Collections;


/* A collection of global variables and methods. */
public static class Globals {
	public static bool checkBounds(Vec2 p_boundary, Vec2 p_point) {
		if(p_point.x >= 0 && p_point.y >= 0 && p_point.x < p_boundary.x && p_point.y < p_boundary.y)
			return true;

		return false;
	}
}



/* Integer based Vector2. */
public class Vec2 {
	int _x, _y;

	public Vec2() {
		_x = _y = 0;
	}

	public Vec2(int p_x, int p_y) {
		_x = p_x;
		_y = p_y;
	}

	public int x {
		get { return _x; }
		set { _x = value; }
	}

	public int y {
		get { return _y; }
		set { _y = value; }
	}
}



/* Integer based Vector3. */
public class Vec3 {
	int _x, _y, _z;

	public Vec3() {
		_x = _y = _z = 0;
	}

	public Vec3(int p_x, int p_y, int p_z) {
		_x = p_x;
		_y = p_y;
		_z = p_z;
	}

	public int x {
		get { return _x; }
		set { _x = value; }
	}

	public int y {
		get { return _y; }
		set { _y = value; }
	}

	public int z {
		get { return _z; }
		set { _z = value; }
	}
}
